# Il paesaggio archeologico dell’Oriente antico - Docs

Documentazione relativa al laboratorio "Il paesaggio archeologico dell’Oriente antico. Studio e analisi dei
sistemi di indagine remote sensing, morfologia e topografia delle
regioni vicino-orientali"

Questo repository contiene la documentazione relativa al sopracitato laboratorio.
In particolare, sono (o saranno) presenti i seguenti documenti:

- Sinossi del corso

- Slides (in pdf) di presentazione delle parti teoriche

- Datasets per esercitazioni pratiche

#### Ispirazioni e fonti per teoria e dati

La parte pratica, soprattutto quella relativa a QGIS, ha preso ispirazione ed è in parte basata su diverse fonti che meritano una menzione:
- [Una breve introduzione al GIS](https://docs.qgis.org/3.16/it/docs/gentle_gis_introduction/index.html)
- [Manuale utente di QGIS 3.16](https://docs.qgis.org/3.16/it/docs/user_manual/index.html)
- [QGIS Tutorials by Ujaval Gandhi](https://www.qgistutorials.com/it/docs/introduction.html)
- [QGIS Introduction Tutorials by Evelyn Uuemaa](https://kevelyn1.github.io/QGIS-Intro/)
- [AnthroYeti QGSI4Arch Youtube Tutorials](https://invidious.namazso.eu/playlist?list=PLqiB3IIUNAnU8vPcuea6A9pB7Y_qQBH1u)

Gran parte dei dati utilizzati nel laboratorio provengono da fonti accessibili liberamente e gratuite. Alcuni dati sono il risultato di ricerche personali e non sono (ancora) disponibili online. 

Elenco di repository da cui si sono scaricati dati (continuamente aggiornati):
- [NaturalEarthData](https://www.naturalearthdata.com/)
- [Ancient World Mapping Center](https://awmc.unc.edu/wordpress/)
- [Geographic Data for Ancient Near Eastern Archaeological Sites](https://www.lingfil.uu.se/research/assyriology/earth/)
- [USGS Earth Explorer](https://earthexplorer.usgs.gov/)
- [CORONA Atlas & Referencing Systems](https://corona.cast.uark.edu/)


#### Licenza
Molte delle risorse a cui la parte pratica su QGIS si è ispirata sono state condivise sotto la licenza CC-BY-SA. Per questo motivo, anche le risorse qui disponibili vengono condivise sotto lo stesso tipo di licenza. 
Si è quindi liberi di:

 - Condividere: copiare e ridistribuire il materiale in qualsiasi supporto o formato. 
 - Adattare: modificare, trasformare e rielaborare sul materiale stesso.
 
Basta solo dare un credito appropriato all'autore per l'opera originale e condividere eventuali materiali sotto la stessa licenza.


##### Struttura del repository (TBA):

